package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @author Cam Carpenter (provided the implementation) Modified February 14th, 2019
 */
public class ArrayListBible implements Bible {

	// The Fields
	String version;
	VerseList verses;
	String title;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses
	 *            All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList importedVerses) {
		verses = new VerseList(importedVerses);
		title = verses.getDescription();
		version = verses.getVersion();

	}

	@Override
	public int getNumberOfVerses() {

		return verses.size();
	}

	@Override
	public String getVersion() {

		return version;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		return true;
	}

	@Override
	public String getVerseText(Reference ref) {
		if (isValid(ref));
		return verses.get(ref.getVerse()).getText();
	}

	public Verse getVerse(Reference ref) {
		if (isValid(ref)){
			int place = ref.getVerse();
			return verses.get(place);
		}
		else {
			return null;
		}
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		return getVerse(new Reference(book, chapter, verse));

	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	// See the Bible interface for the documentation of these methods.
	// Do not over think these methods. All three should be pretty
	// straightforward to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they
	// work better.
	// At that stage you will create another class to facilitate searching and
	// use it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	@Override
	public VerseList getAllVerses() {
		System.out.println("Version: " + version);
		return verses;
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList versesContaining = new VerseList(verses);
		versesContaining.clear();
		for (Verse verse: verses) {
			if (verse.getText().contains(phrase)) {
				versesContaining.add(verse);
			}
			return versesContaining;	
		}
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		// TODO Implement me: Stage 4
		return null;
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList versesObtained = new VerseList(verses);
		versesObtained.clear();
		for (Reference ref: references) {
			versesObtained.add(verses.get(ref.getVerse()));
		}
		return versesObtained;
	}
	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 7.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented
	// by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		// TODO Implement me: Stage 7
		return -1;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}
}
